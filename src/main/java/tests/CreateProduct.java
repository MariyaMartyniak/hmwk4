package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;


import java.io.File;

public class CreateProduct {
    WebDriver driver;

    @BeforeClass
    public void setup() {
        System.setProperty(
                "webdriver.chrome.driver",
                new File(CreateProduct.class.getResource("/chromedriver.exe").getFile()).getPath());
        driver = new ChromeDriver();
    }


    @Test
    public void loginTest() {
        driver.navigate().to("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        driver.findElement(By.id("email")).sendKeys("webinar.test@gmail.com");
        driver.findElement(By.id("passwd")).sendKeys("Xcg7299bnSmMuRLp9ITw");
        driver.findElement(By.name("submitLogin")).click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.titleContains("Dashboard"));
    }


    @Test(dependsOnMethods = "loginTest")
    public void clickCategorySubmenu(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 35);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Каталог']")));

        WebElement catalogMenuElement = driver.findElement(By.xpath("//span[text()='Каталог']"));
        Actions action = new Actions(driver);


        action.moveToElement(catalogMenuElement).build().perform();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='товары']")));
        catalogMenuElement.findElement(By.xpath("//*[text()='товары']")).click();
    }


    public void clickItemsSubmenu() {
        By catalogMenu = By.xpath("//span[text()='Каталог']");
        By itemsSubmenu = By.xpath("//*[text()='товары']");
        WebDriverWait wait = new WebDriverWait(driver, 8);
        wait.until(ExpectedConditions.elementToBeClickable(catalogMenu));

        WebElement catalogMenuElement = driver.findElement(catalogMenu);

        Actions action = new Actions(driver);
        action.moveToElement(catalogMenuElement).build().perform();
        wait.until(ExpectedConditions.elementToBeClickable(itemsSubmenu));
        catalogMenuElement.findElement(itemsSubmenu).click();
    }


}







