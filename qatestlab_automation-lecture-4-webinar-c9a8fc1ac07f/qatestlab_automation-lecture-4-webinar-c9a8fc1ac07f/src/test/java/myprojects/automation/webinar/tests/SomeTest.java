package myprojects.automation.webinar.tests;

import myprojects.automation.webinar.tests.utils.BaseTest;
import myprojects.automation.webinar.tests.utils.WebDriverLogger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static myprojects.automation.webinar.tests.utils.BaseTest.getDriver;


public class SomeTest {
    WebDriver driver;
    private static String itemName;
    private static String itemsPrice;
    private static Integer itemCount;


    public void setItemsPrice(String itemsPrice) {
        this.itemsPrice = itemsPrice;
    }

    public static String getItemsPrice() {
        return itemsPrice;
    }

    public static String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public static Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }


    @Parameters("browser")
    @BeforeClass
    public void beforeClass(String browser) {
        if (browser.equalsIgnoreCase("firefox")) {
            System.setProperty(
                    "webdriver.gecko.driver",
                    new File(BaseTest.class.getResource("/geckodriver.exe").getFile()).getPath());
            driver = new FirefoxDriver();

        } else if (browser.equalsIgnoreCase("ie")) {
            System.setProperty("webdriver.ie.driver",
                    new File(BaseTest.class.getResource("/IEDriverServer.exe").getFile()).getPath());
            driver = new InternetExplorerDriver();

        } else if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty(
                    "webdriver.chrome.driver",
                    new File(BaseTest.class.getResource("/chromedriver.exe").getFile()).getPath());
            driver = new ChromeDriver();

        } else {
        }
        EventFiringWebDriver wrappedDriver = new EventFiringWebDriver(driver);
        driver = wrappedDriver.register(new WebDriverLogger());
    }

//    @BeforeClass
//    public void getConfiguredDriver(WebDriver driver) {
//        EventFiringWebDriver wrappedDriver = new EventFiringWebDriver(driver);
//        wrappedDriver.register(new WebDriverLogger());
//    }

//    @BeforeClass
//    public void setup() {
//        System.setProperty(
//                "webdriver.chrome.driver",
//                new File(SomeTest.class.getResource("/chromedriver.exe").getFile()).getPath());
//        driver = new ChromeDriver();
//    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @DataProvider
    public Object[][] getLogin() {
        return new String[][]{
                {"webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw"}
        };
    }


    @Test(dataProvider = "getLogin")
    public void loginTest(String login, String password) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        driver.navigate().to("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        driver.manage().window().maximize();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("email")));
        driver.findElement(By.id("email")).sendKeys(login);
        driver.findElement(By.id("passwd")).sendKeys(password);
        driver.findElement(By.name("submitLogin")).click();
        wait.until(ExpectedConditions.titleContains("Dashboard"));
    }

    @Test(dependsOnMethods = "loginTest")
    public void clickItemsSubmenu() {
        By catalogMenu = By.xpath("//*[@id=\"subtab-AdminCatalog\"]/a/span");
        By itemsSubmenu = By.xpath("//*[@id=\"subtab-AdminProducts\"]/a");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(catalogMenu));

        WebElement catalogMenuElement = driver.findElement(catalogMenu);
        Actions action = new Actions(driver);

        action.moveToElement(catalogMenuElement).build().perform();
        wait.until(ExpectedConditions.elementToBeClickable(itemsSubmenu));
        catalogMenuElement.findElement(itemsSubmenu).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("products-catalog")));
    }

    @Test(dependsOnMethods = "clickItemsSubmenu")
    public void addNewItem() {
        WebDriverWait wait = new WebDriverWait(driver, 5);

        WebElement addNewItemButton = driver.findElement(By.xpath("//*[@id='page-header-desc-configuration-add']/span"));
        addNewItemButton.click();
        By categoryNameInputField = By.id("form_step1_name_1");
        wait.until(ExpectedConditions.visibilityOfElementLocated(categoryNameInputField));


        //  Random random = new Random();

        String[] itemNames = {"Dress", "Skirt", "T-Shirt", "Pans", "Shoes"};
        Random random = new Random();
        itemName = itemNames[random.nextInt(itemNames.length)];
        driver.findElement(categoryNameInputField).sendKeys(itemName);
        setItemName(itemName);
        System.out.println(getItemName());
        //  itemName = "Shoes";
        //  driver.findElement(categoryNameInputField).sendKeys(itemName);
        setItemName(itemName);


        driver.findElement(By.xpath("//*[@id=\"tab_step3\"]/a")).click();
        WebElement itemsCount = driver.findElement(By.id("form_step3_qty_0"));
        itemsCount.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        Integer itemCount = random.nextInt(100) + 1;
        setItemCount(itemCount);
        itemsCount.sendKeys(Integer.toString(itemCount));

        driver.findElement(By.xpath("//*[@id=\"tab_step2\"]/a\n")).click();
        WebElement priceInputField = driver.findElement(By.id("form_step2_price"));
        priceInputField.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        double minPrice = 0.1;
        double maxXPrice = 100;
        double itemsPriceD = random.nextDouble() * (maxXPrice - minPrice) + minPrice;
        double itemsPriceR = Math.round(itemsPriceD * 100.0) / 100.0;
        String itemsPrice = Double.toString(itemsPriceR);
        setItemsPrice(itemsPrice);
        priceInputField.sendKeys(itemsPrice);
        System.out.println(itemsPrice);
        driver.findElement(By.className("switch-input")).click();

        driver.findElement(By.xpath("//span[text()[contains(.,'Сохранить')]]")).click();

    }
}


//    public void scrollPage() {
//        WebElement addCategoryButton = driver.findElement(By.id("add-category-button"));
//        JavascriptExecutor executor = (JavascriptExecutor) driver;
//        executor.executeScript("arguments[0].scrollIntoView(true);", addCategoryButton);
//    }


//    @Test(dependsOnMethods = "loginTest")
//    public void checkStatsFilterButtons() {
//        Reporter.log("Open Statistics section <br />");
//        driver.findElement(By.cssSelector("#subtab-AdminStats a")).click();
//
//        Reporter.log("Check filter buttons amount <br />");
//        List<WebElement> filterButtons = driver.findElements(By.cssSelector("#calendar_form .btn-group button"));
//        Assert.assertEquals(
//                filterButtons.size(), 6,
//                "Wrong filter buttons amount"
//        );
//
//        Reporter.log("Check filter buttons visibility <br />");
//        SoftAssert assertVisibility = new SoftAssert();
//        for (WebElement btn : filterButtons) {
//            assertVisibility.assertTrue(
//                    btn.isDisplayed(),
//                    String.format("Filter button %s is not visible.", btn.getAttribute("name"))
//            );
//        }
//        assertVisibility.assertAll();
//    }
//}
//
//    @Parameters({"nameParam"})
//    @Test
//    public void test3(String name) {
//        Reporter.log(String.format("Hello, %s!", name));
//
//    }

